/************************************************************************
 * GoogleSparseMapInterface.hh                                          *
 * Interface to use for benchmarking google::sparse_hash_map            *
 *                                                                      *
 * Author: Tobias Kappé <tobias.kappe@cern.ch>                          *
 * Copyright (C) 2015 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#ifndef HAVE_GOOGLESPARSEMAPINTERFACE_H
#define HAVE_GOOGLESPARSEMAPINTERFACE_H

#include <google/sparse_hash_map>
#include "MapInterface.hh"

using google::sparse_hash_map;

template <typename Key, typename Value>
class GoogleSparseMapInterface : public MapInterface<Key, Value> {
private:
    google::sparse_hash_map<Key, Value>* instance;

public:
    GoogleSparseMapInterface();
    virtual ~GoogleSparseMapInterface();
    virtual void setItem(Key key, Value value);
    virtual Value* getItem(Key key);
    virtual void deleteItem(Key key);
    virtual void iterate();
    virtual char const* getName();
};

extern "C" {
void* make_key_string();
void* make_key_longlong();
}

#endif

// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
