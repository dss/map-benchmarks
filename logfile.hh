/************************************************************************
 * logfile.hh                                                           *
 * Common types for a benchmark log file.                               *
 *                                                                      *
 * Author: Tobias Kappé <tobias.kappe@cern.ch>                          *
 * Copyright (C) 2015 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#ifndef HAVE_LOGFILE_HH
#define HAVE_LOGFILE_HH

#ifdef USE_STRING_KEY
typedef std::string map_key_t;
#else
typedef long long map_key_t;
#endif

typedef enum : unsigned char {
    KEY_UNDEFINED = 0,
    KEY_LONGLONG = 1,
    KEY_STRING = 2
} key_type_t;

typedef struct {
    key_type_t key_type;
    size_t value_size;
    size_t key_size;
    size_t entries;
    size_t total_operations;
    bool alphanumeric;
} __attribute__((packed)) logfile_conf_t;

typedef enum : unsigned char {
    OPERATION_SET = 0,
    OPERATION_GET = 1,
    OPERATION_GET_RANDOM = 2,
    OPERATION_DELETE = 3,
    OPERATION_DELETE_RANDOM = 4,
    OPERATION_ITERATE = 5
} operation_type_t;

typedef struct {
    operation_type_t operation;
    size_t repeats;
} __attribute__((packed)) operation_t;

#endif
