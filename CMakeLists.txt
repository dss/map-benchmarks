# For some reason, CMake ignores these. We therefore import them manually...
set(CMAKE_C_COMPILER $ENV{CC})
set(CMAKE_CXX_COMPILER $ENV{CXX})

cmake_minimum_required(VERSION 2.8.11)

project(benchmark)

# Later versions of CMake can detect compiler features easily, but this
# version cannot.
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.4")
    message(FATAL_ERROR "Insufficient gcc version")
  endif()
else()
  message(WARNING "Did not find gcc; other compilers are untested.")
  message(WARNING "Proceed with caution.")
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS_DEBUG} -Wall -Wextra -std=c++0x")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS_DEBUG} -Wall -Wextra")
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

add_subdirectory(PersistentHashtable)

add_library(PersistentHashtableInterface SHARED PersistentHashtableInterface.cc)

target_link_libraries(PersistentHashtableInterface LINK_PUBLIC persistenthashtable)
set_target_properties(PersistentHashtableInterface PROPERTIES PREFIX "")

add_library(StdMapInterface SHARED StdMapInterface.cc)
set_target_properties(StdMapInterface PROPERTIES PREFIX "")

find_package(sparsehash)
if (SPARSEHASH_FOUND)
    add_library(GoogleDenseMapInterface SHARED GoogleDenseMapInterface.cc)
    set_target_properties(GoogleDenseMapInterface PROPERTIES PREFIX "")

    add_library(GoogleSparseMapInterface SHARED GoogleSparseMapInterface.cc)
    set_target_properties(GoogleSparseMapInterface PROPERTIES PREFIX "")
else()
    message(WARNING "Could not find sparsehash. "
                    "I will build not the shared libraries to benchmark it.")
endif()

find_package(procps REQUIRED)
if (NOT PROCPS_FOUND)
    message(FATAL_ERROR "Could not find procps.")
endif ()

find_package(Threads REQUIRED)
if (NOT Threads_FOUND)
	message(FATAL_ERROR "Could not find Threads.")
endif ()

include_directories(${PROCPS_INCLUDE_DIR})
set(LIBS ${LIBS} ${PROCPS_LIBRARY})

add_executable(benchmark-longlong benchmark.cpp)
target_compile_definitions(benchmark-longlong PUBLIC -DUSE_LONGLONG_KEY)
target_link_libraries(benchmark-longlong LINK_PUBLIC ${LIBS})
target_link_libraries(benchmark-longlong LINK_PUBLIC dl)
target_link_libraries(benchmark-longlong LINK_PUBLIC ${CMAKE_THREAD_LIBS_INIT})

add_executable(benchmark-string benchmark.cpp)
target_compile_definitions(benchmark-string PUBLIC -DUSE_STRING_KEY)
target_link_libraries(benchmark-string LINK_PUBLIC ${LIBS})
target_link_libraries(benchmark-string LINK_PUBLIC dl)
target_link_libraries(benchmark-string LINK_PUBLIC ${CMAKE_THREAD_LIBS_INIT})

add_executable(genlogfile genlogfile.cpp)

