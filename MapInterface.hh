/************************************************************************
 * MapInterface.hh                                                      *
 * Common interface for all map interfaces.                             *
 *                                                                      *
 * Author: Tobias Kappé <tobias.kappe@cern.ch>                          *
 * Copyright (C) 2015 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#ifndef HAVE_MAPINTERFACE_H
#define HAVE_MAPINTERFACE_H

#include "logfile.hh"

static const std::string empty_key_string = std::string("");
static const std::string deleted_key_string = std::string("<deleted>");

static const long long empty_key_longlong = 0;
static const long long deleted_key_longlong = -1;

template <typename Key, typename Value>
class MapInterface {
public:
    virtual void setItem(Key key, Value value) = 0;
    virtual Value* getItem(Key key) = 0;
    virtual void deleteItem(Key key) = 0;
    virtual void iterate() = 0;
    virtual char const* getName() = 0;
    virtual ~MapInterface() {};
    virtual void setupParameters(logfile_conf_t&) {};
    virtual void writeFingerprint(std::ofstream&) {};
};
#endif

// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
