#include <dlfcn.h>
#include <fstream>
#include <iostream>
#include <proc/readproc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <sys/times.h>

#include "logfile.hh"
#include "MapInterface.hh"

#ifdef USE_STRING_KEY
const key_type_t key_expect = KEY_STRING;
#else
const key_type_t key_expect = KEY_LONGLONG;
#endif

void read_into(std::ifstream& source, std::string& target, size_t count)
{
    static const size_t buffer_size = 1024;
    static char buffer[buffer_size];

    target.clear();
    target.reserve(count);

    size_t read = 0;
    while(read < count) {
        size_t todo = std::min(count, buffer_size);
        source.read(buffer, todo);
        target.append(buffer, todo);

        read += todo;
    }
}

int main(int argc, char** argv)
{
    bool write_fingerprints = false;
    std::ofstream fingerprints;

    if(argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <log file> <interface> "
                  << "[fingerprint file]" << std::endl;
        exit(1);
    }

    if(argc >= 4) {
        write_fingerprints = true;
        fingerprints.open(argv[3]);

        if(fingerprints.fail()) {
            std::cerr << "Could not open fingerprint file '" << argv[2]
                      << "' for writing." << std::endl;
            exit(1);
        }
    }

    void* interface_handle = dlopen(argv[2], RTLD_NOW);

    if(!interface_handle) {
        std::cerr << "Could not open interface library" << std::endl;
        std::cerr << dlerror() << std::endl;
        exit(1);
    }

    void* (*interface_factory)() = (void*(*)())
#ifdef USE_STRING_KEY
        dlsym(interface_handle, "make_key_string");
#else
        dlsym(interface_handle, "make_key_longlong");
#endif

    if(!interface_factory) {
        std::cerr << "Could not find factory method" << std::endl;
        std::cerr << dlerror() << std::endl;
        exit(1);
    }

    MapInterface<map_key_t, std::string>* interface =
        (MapInterface<map_key_t, std::string>*) interface_factory();

    std::ifstream logfile(argv[1], std::ios::in | std::ios::binary);
    if(logfile.fail()) {
        std::cerr << "Could not open log file '" << argv[1]
                  << "' for reading." << std::endl;
        exit(1);
    }

    logfile_conf_t conf;
    logfile.read((char*) &conf, sizeof(conf));

    if(conf.key_type != key_expect) {
        std::cerr << "Key type mismatch." << std::endl;
        exit(1);
    }

    interface->setupParameters(conf);

    printf("%-24s %-8s %-8s %-8s %-8s\n",
           "structure", "user", "kernel", "total", "memory (KB)");

    struct tms time_start, time_end;
    if(times(&time_start) == -1) {
        std::cerr << "Could not get system time." << std::endl;
        exit(1);
    }

    size_t operations = 0;
    while(operations < conf.total_operations) {
        operation_t operation;
        logfile.read((char*) &operation, sizeof(operation));

        for(size_t j = 0; j < operation.repeats; j++) {
            switch(operation.operation) {
            case OPERATION_SET:
            case OPERATION_GET:
            case OPERATION_GET_RANDOM:
            case OPERATION_DELETE:
            case OPERATION_DELETE_RANDOM:
            case OPERATION_ITERATE:
                break;
            default:
                std::cerr << "Unknown operation code "
                          << operation.operation << std::endl;
                exit(1);
                break;
            }

            map_key_t key;
            std::string value;

            switch(operation.operation) {
            case OPERATION_SET:
            case OPERATION_GET:
            case OPERATION_GET_RANDOM:
            case OPERATION_DELETE:
            case OPERATION_DELETE_RANDOM:
#ifdef USE_STRING_KEY
                read_into(logfile, key, conf.key_size);
#else
                logfile.read((char*) &key, sizeof(key));
#endif
                break;
            default:
                break;
            }

            switch(operation.operation) {
            case OPERATION_SET:
                read_into(logfile, value, conf.value_size);
                interface->setItem(key, value);
                break;
            case OPERATION_GET:
            case OPERATION_GET_RANDOM:
                interface->getItem(key);
                break;
            case OPERATION_DELETE:
            case OPERATION_DELETE_RANDOM:
                interface->deleteItem(key);
                break;
            case OPERATION_ITERATE:
                interface->iterate();
                break;
            }

            if(write_fingerprints)
                interface->writeFingerprint(fingerprints);
        }

        operations += operation.repeats;
    }

    if(times(&time_end) == -1) {
        std::cerr << "Could not get system time." << std::endl;
        exit(1);
    }

    // Only measure memory consumption at the end. This may or may not be
    // interesting depending on the instructions in the log file.
    struct proc_t mem_end;
    look_up_our_self(&mem_end);

    const double clocks_per_sec = sysconf(_SC_CLK_TCK);
    double time_user = (time_end.tms_utime-time_start.tms_utime)/clocks_per_sec;
    double time_kernel = (time_end.tms_stime-time_start.tms_stime)/clocks_per_sec;
    double time_total = time_user + time_kernel;

    printf("%-24s %-8.2f %-8.2f %-8.2f %-8lu\n", interface->getName(),
           time_user, time_kernel, time_total, mem_end.vsize >> 10);

    delete interface;

    dlclose(interface_handle);

    return 0;
}

// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
