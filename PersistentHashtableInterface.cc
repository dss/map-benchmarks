/************************************************************************
 * PersistentHashtableInterface.cc                                      *
 * Interface to use for benchmarking PersistentHasthable                *
 *                                                                      *
 * Author: Tobias Kappé <tobias.kappe@cern.ch>                          *
 * Copyright (C) 2015 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#include <string>
#include <cstring>
#include <hashtable.h>
#include <fstream>

#include "PersistentHashtableInterface.hh"
#include "logfile.hh"

template<>
long long my_hash<std::string>(const void* contents)
{
    const char* str = ((std::string*) contents)->c_str();

    long long ret = 0;
    int pos = 0;

    while(*str) {
        ret ^= (*str) << (pos*sizeof(char));
        str++;
        pos = (pos + 1) % sizeof(long long);
    }

    return ret;
}

template<>
long long my_hash<long long>(const void* contents)
{
    return *((long long*) contents);
}

template<>
int my_cmp<std::string>(const void* a, const void* b)
{
    return strcmp(
        ((std::string*) a)->c_str(),
        ((std::string*) b)->c_str()
    );
}

template<>
int my_cmp<long long>(const void* a, const void* b)
{
    return *((long long*) a) < *((long long*) b);
}

template<typename Key, typename Value>
PersistentHashtableInterface<Key, Value>::PersistentHashtableInterface()
{
}

template<typename Key, typename Value>
PersistentHashtableInterface<Key, Value>::~PersistentHashtableInterface()
{
    delete instance;
}

template<typename Key, typename Value>
void PersistentHashtableInterface<Key, Value>::writeFingerprint(std::ofstream& output)
{
    instance->writeFingerprint(output);
}

template <typename Key, typename Value>
void PersistentHashtableInterface<Key, Value>::
    setupParameters(logfile_conf_t& conf)
{
    hash_func_t hash = &my_hash<Key>;
    comp_func_t comp = &my_cmp<Key>;

    instance = new PersistentHashtable<Key, Value>(conf.entries, hash, comp);
}

template<typename Key, typename Value>
char const* PersistentHashtableInterface<Key, Value>::getName()
{
    return "PersistentHashtable";
}

template<typename Key, typename Value>
void PersistentHashtableInterface<Key, Value>::setItem(Key key, Value value)
{
    (*instance)[key] = value;
}

template<typename Key, typename Value>
Value* PersistentHashtableInterface<Key, Value>::getItem(Key key)
{
    persistent_hashtable_entry_t* entry = hashtable_get(instance->instance, &key);
    return entry ? (Value*) &entry->value : NULL;
}

template<typename Key, typename Value>
void PersistentHashtableInterface<Key, Value>::iterate()
{
    for(typename PersistentHashtable<Key, Value>::iterator it = instance->begin();
        it != instance->end(); ++it) {
        asm(""); // Do not optimize.
    }
}

template<typename Key, typename Value>
void PersistentHashtableInterface<Key, Value>::deleteItem(Key key)
{
    instance->erase(key);
}

void* make_key_string()
{
    return new PersistentHashtableInterface<std::string, std::string>();
}

void* make_key_longlong()
{
    return new PersistentHashtableInterface<long long, std::string>();
}

// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
