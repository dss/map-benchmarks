import sys
import numpy

filename = sys.argv[1]

N = 1000
with open(filename, 'r') as handle:
    sum_run = numpy.zeros(4)
    run_length = 0.0

    for line in handle:
        if not line.strip():
            print("%d %f %f %f %d" % ((N,) + tuple(sum_run/run_length)))

            sum_run = numpy.zeros(4)
            run_length = 0.0
            N += 1000
        else:
            sum_run = sum_run + numpy.array([float(w) for w in line.strip().split(' ')[1:] if w])
            run_length += 1


