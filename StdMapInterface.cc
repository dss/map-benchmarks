/************************************************************************
 * StdMapInterface.cc                                                   *
 * Interface to use for benchmarking std::map                           *
 *                                                                      *
 * Author: Tobias Kappé <tobias.kappe@cern.ch>                          *
 * Copyright (C) 2015 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#include <map>
#include <string>
#include "StdMapInterface.hh"

template <typename Key, typename Value>
StdMapInterface<Key, Value>::StdMapInterface()
{
    instance = new std::map<Key, Value>();
}

template <typename Key, typename Value>
StdMapInterface<Key, Value>::~StdMapInterface()
{
    delete instance;
}

template <typename Key, typename Value>
void StdMapInterface<Key, Value>::setItem(Key key, Value value)
{
    (*instance)[key] = value;
}

template <typename Key, typename Value>
Value* StdMapInterface<Key, Value>::getItem(Key key)
{
    return &(*instance)[key];
}

template <typename Key, typename Value>
void StdMapInterface<Key, Value>::deleteItem(Key key)
{
    instance->erase(key);
}

template <typename Key, typename Value>
void StdMapInterface<Key, Value>::iterate()
{
    typename std::map<Key, Value>::iterator it;

    for(it = instance->begin(); it != instance->end(); ++it) {
        asm(""); // Do not optimize
    }
}

template <typename Key, typename Value>
char const* StdMapInterface<Key, Value>::getName()
{
    return "std::map";
}

void* make_key_string()
{
    return new StdMapInterface<std::string, std::string>();
}

void* make_key_longlong()
{
    return new StdMapInterface<long long, std::string>();
}

// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
