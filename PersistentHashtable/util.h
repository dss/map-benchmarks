/************************************************************************
 * util.c                                                               *
 * Common utility functions.                                            *
 *                                                                      *
 * Author: Tobias Kappé <tobias.kappe@cern.ch>                          *
 * Copyright (C) 2015 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#ifndef HAVE_UTIL_H
#define HAVE_UTIL_H

#include <inttypes.h>

// Dummy macro to annotate blocks that are supposed to be atomic
#define ATOMIC

inline void swap(const void** a, const void** b);
inline int max(int a, int b);
inline void* align_next_page_boundary(void* start);

#endif

// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
