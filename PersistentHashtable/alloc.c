/************************************************************************
 * alloc.c                                                              *
 * Stubs pmalloc and pfree to compile the code outside Mnemosyne.       *
 *                                                                      *
 * Author: Tobias Kappé <tobias.kappe@cern.ch>                          *
 * Copyright (C) 2015 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#include <stdlib.h>

#include "alloc.h"

// Dummy function for persistent memory allocation.
void persistent* pmalloc(size_t size)
{
    return malloc(size);
}

// Dummy function for persistent memory deallocation.
void pfree(void persistent* ptr)
{
    free(ptr);
}
