/************************************************************************
 * util.c                                                               *
 * Common utility functions.                                            *
 *                                                                      *
 * Author: Tobias Kappé <tobias.kappe@cern.ch>                          *
 * Copyright (C) 2015 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#include <inttypes.h>
#include <unistd.h>

#include "util.h"

inline int max(int a, int b)
{
    return a > b ? a : b;
}

inline void swap(const void** a, const void** b)
{
    const void* tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

inline void* align_next_page_boundary(void* start)
{
    uintptr_t pagesize = getpagesize();

    return (void*) (
        ((uintptr_t) start + (pagesize)) &
        (uintptr_t) ~(pagesize-1)
    );
}

// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
