#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "logfile.hh"

const size_t BUFFER_SIZE = 8192;
static unsigned char buffer[BUFFER_SIZE];

static void read_conffile(
    std::ifstream& conffile,
    logfile_conf_t& conf,
    std::vector<operation_t>& operations
)
{
    while(!conffile.eof()) {
        std::string line;
        std::getline(conffile, line);

        if(line.empty()) {
            break;
        }

        std::istringstream iss(line);
        std::string key, value;

        iss >> key >> value;

        if(key.compare("key_type") == 0) {
            if(value.compare("longlong") == 0) {
                conf.key_type = KEY_LONGLONG;
            } else if(value.compare("string") == 0) {
                conf.key_type = KEY_STRING;
            } else {
                std::cerr << "Unknown key type '"
                          << value << "'" << std::endl;
                exit(1);
            }
        } else if(key.compare("value_size") == 0) {
            conf.value_size = atol(value.c_str());
        } else if(key.compare("key_size") == 0) {
            conf.key_size = atol(value.c_str());
        } else if(key.compare("entries") == 0) {
            conf.entries = atol(value.c_str());
        } else if(key.compare("alphanumeric") == 0) {
            conf.alphanumeric = value.compare("true") == 0;
        } else {
            std::cerr << "Unknown configuration key '"
                      << value << "'" << std::endl;
            exit(1);
        }
    }

    if(conf.key_type == KEY_UNDEFINED) {
        std::cerr << "Key type not specified" << std::endl;
    }

    if(conf.value_size == 0) {
        std::cerr << "Value size not specified" << std::endl;
        exit(1);
    }

    if(conf.key_type == KEY_STRING && conf.key_size == 0) {
        std::cerr << "Key size not specified" << std::endl;
        exit(1);
    }

    if(conf.key_size+1 > BUFFER_SIZE) {
        std::cerr << "Keys do not fit in buffer" << std::endl;
        exit(1);
    }

    if(conf.key_type == KEY_LONGLONG) {
        conf.key_size = sizeof(long long);
    }

    if(conf.entries == 0) {
        std::cerr << "Number of entries not specified" << std::endl;
        exit(1);
    }

    conf.total_operations = 0;
    while(!conffile.eof()) {
        operation_t operation;
        std::string operation_name;

        memset(&operation, 0, sizeof(operation));

        std::string line;
        std::getline(conffile, line);

        if(line.empty()) {
            break;
        }

        std::istringstream iss(line);
        std::string key, value;

        size_t repeats;
        iss >> operation_name >> repeats;

        // We cannot bind the field directly because of packing.
        operation.repeats = repeats;

        if(operation_name.compare("set") == 0) {
            operation.operation = OPERATION_SET;
        } else if(operation_name.compare("get") == 0) {
            operation.operation = OPERATION_GET;
        } else if(operation_name.compare("get-random") == 0) {
            operation.operation = OPERATION_GET_RANDOM;
        } else if(operation_name.compare("delete") == 0) {
            operation.operation = OPERATION_DELETE;
        } else if(operation_name.compare("delete-random") == 0) {
            operation.operation = OPERATION_DELETE_RANDOM;
        } else if(operation_name.compare("iterate") == 0) {
            operation.operation = OPERATION_ITERATE;
        } else {
            std::cerr << "Unknown operation '" << operation_name
                      << "'" << std::endl;
            exit(1);
        }

        operations.push_back(operation);
        conf.total_operations += operation.repeats;
    }
}

static void write_random(
    std::ofstream& logfile,
    std::ifstream& random,
    size_t length,
    bool alphanumeric
) {
    size_t read = 0;
    while(read < length) {
        size_t todo = std::min(length-read, BUFFER_SIZE);
        random.read((char*) buffer, todo);

        if(alphanumeric) {
            for(size_t j = 0; j < length; j++) {
                buffer[j] = (buffer[j] % 26) + 'a';
            }
        }

        logfile.write((char*) buffer, todo);
        read += todo;
    }
}

static void write_random_key(
    std::ofstream& logfile,
    std::ifstream& random,
    logfile_conf_t& conf
)
{
    write_random(
        logfile,
        random,
        conf.key_size,
        conf.alphanumeric && conf.key_type != KEY_LONGLONG
    );
}

static void write_random_value(
    std::ofstream& logfile,
    std::ifstream& random,
    logfile_conf_t& conf
)
{
    write_random(
        logfile,
        random,
        conf.value_size,
        conf.alphanumeric
    );
}

static void generate_logfile(
    std::ofstream& logfile,
    logfile_conf_t& conf,
    std::vector<operation_t>& operations
)
{
    // The key size only makes sense for hashtables with string as key type,
    // but we write it out anyway because it eases reading the file later.
    logfile.write((char*) &conf, sizeof(conf));

    std::ifstream random("/dev/urandom", std::ios::in | std::ios::binary);
    if(random.fail()) {
        std::cerr << "Could not open randomness source." << std::endl;
        exit(1);
    }

    unsigned int seed;
    random.read((char*) &seed, sizeof(seed));
    srand(seed);

    // Used to keep track of keys added to the map.
    std::vector<unsigned char*> contents;
    int victim;

    for(size_t i = 0; i < operations.size(); i++) {
        operation_t operation = operations[i];
        logfile.write((char*) &operation, sizeof(operation));
        for(size_t i = 0; i < operation.repeats; i++) {
            switch(operation.operation) {
            case OPERATION_SET:
                write_random_key(logfile, random, conf);

                contents.push_back(
                    (unsigned char*) strndup((char*) buffer,
                    conf.key_size)
                );

                write_random_value(logfile, random, conf);
                break;
            case OPERATION_GET:
                victim = rand() % contents.size();
                logfile.write((char*) contents[victim], conf.key_size);
                break;
            case OPERATION_DELETE:
                victim = rand() % contents.size();
                logfile.write((char*) contents[victim], conf.key_size);
                contents.erase(contents.begin() + victim);
                break;
            case OPERATION_GET_RANDOM:
            case OPERATION_DELETE_RANDOM:
                write_random_key(logfile, random, conf);
                break;
            case OPERATION_ITERATE:
                break;
            }
        }
    }
}

int main(int argc, char** argv)
{
    logfile_conf_t conf;
    memset(&conf, 0, sizeof(conf));

    std::vector<operation_t> operations;

    if(argc < 3) {
        std::cerr << "Usage: " << argv[0]
                  << " <configuration file> <log file>"
                  << std::endl;
        exit(1);
    }

    std::ifstream conffile(argv[1]);
    if(conffile.fail()) {
        std::cerr << "Could not open configuration file '"
                  << argv[1] << "' for reading" << std::endl;
        exit(1);
    }

    std::ofstream logfile(argv[2], std::ios::out | std::ios::binary);

    if(logfile.fail()) {
        std::cerr << "Could not open logfile '"
                  << argv[2] << "' for writing"
                  << std::endl;
        exit(1);
    }

    read_conffile(conffile, conf, operations);

    std::cout << "Generating logfile" << std::endl;
    generate_logfile(logfile, conf, operations);
}
