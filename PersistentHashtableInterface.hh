/************************************************************************
 * PersistentHashtableInterface.hh                                      *
 * Interface to use for benchmarking PersistentHashtable                *
 *                                                                      *
 * Author: Tobias Kappé <tobias.kappe@cern.ch>                          *
 * Copyright (C) 2015 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#ifndef HAVE_HASHTABLEINTERFACE_H
#define HAVE_HASHTABLEINTERFACE_H

#include <string>
#include <zlib.h>
#include <fstream>
#include <stack>

#include "PersistentHashtable/PersistentHashtable.hh"
#include "MapInterface.hh"
#include "logfile.hh"

template<typename Key, typename Value>
class PersistentHashtableInterface : public MapInterface<Key, Value>
{
protected:
    PersistentHashtable<Key, Value>* instance;

public:
    PersistentHashtableInterface();
    virtual ~PersistentHashtableInterface();

    virtual void setupParameters(logfile_conf_t& conf);

    virtual void writeFingerprint(std::ofstream& output);

    virtual char const* getName();
    virtual void iterate();
    virtual void setItem(Key key, Value value);
    virtual Value* getItem(Key key);
    virtual void deleteItem(Key key);
};

template<typename Key>
long long my_hash(const void* contents);

template<typename Key>
int my_cmp(const void* a, const void* b);

extern "C" {
void* make_key_string();
void* make_key_longlong();
}

#endif

// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
